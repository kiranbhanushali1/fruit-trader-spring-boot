package com.company.fruittraderspringboot.service;

import com.company.fruittraderspringboot.exception.TraderNotFoundException;
import com.company.fruittraderspringboot.model.Trader;
import com.company.fruittraderspringboot.model.Transaction;
import com.company.fruittraderspringboot.repository.TraderRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;


@SpringBootTest
class TraderServiceTest {

        @Autowired
        TraderService traderService;
        @MockBean
        TraderRepository traderRepository;

        @Test
        public void createUser(){
                Trader trader = Trader.builder().traderName("Kiran").build();
                when(traderRepository.save(trader)).thenReturn(trader);
                assertEquals(trader,traderService.saveTrader(trader));
        }

        /*
        @Test
        public void buy(){
                Trader trader = Trader.builder().traderName("Jenish").build();
                Transaction transaction = Transaction.builder().trader(trader).fruitName("Banana").weight(130).price(20).build();
                traderService.buyFruit(transaction);
        }

        @Test
        public void getProfit() throws TraderNotFoundException {
                System.out.println(traderService.getProfit(1L));
        }

        @Test
        public void updateProfit() throws TraderNotFoundException{
                traderService.updateProfit(1L,100);
                getProfit();
        }
*/
}