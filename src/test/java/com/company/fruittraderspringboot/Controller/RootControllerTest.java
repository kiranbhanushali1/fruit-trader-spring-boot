package com.company.fruittraderspringboot.Controller;

import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;

class RootControllerTest {
        @Test
        void root(){
                RootController rootController = new RootController();
                String response = rootController.root().getBody().toString();
                assertEquals("Application is Running",response);

        }

}