package com.company.fruittraderspringboot.Controller;

import com.company.fruittraderspringboot.dto.TransactionDto;
import com.company.fruittraderspringboot.exception.*;
import com.company.fruittraderspringboot.model.Trader;
import com.company.fruittraderspringboot.model.Transaction;
import com.company.fruittraderspringboot.service.TraderService;
import org.modelmapper.ModelMapper;
import org.modelmapper.Provider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
public class TraderController {
        @Autowired
        TraderService traderService;
        @Autowired
        ModelMapper modelMapper;



        @PostMapping("/trader")
        public ResponseEntity createTrader(@RequestBody Map<String,Object> body) throws TraderNameNullException {
                Trader trader  = Trader.builder().customBuilder(body).build();
                traderService.saveTrader(trader);
                return new ResponseEntity("Trader Created Successfully "+ trader , HttpStatus.OK);
        }
        @PostMapping(value = "/trader/{traderId}/buy")
        public ResponseEntity buy(@PathVariable Long traderId , @Valid @RequestBody TransactionDto transactionDto) throws FruitNameNullException, NegativePriceException, NegativeWeightException, TraderNotFoundException {
                transactionDto.setTraderId(traderId);
                Transaction transaction = modelMapper.map(transactionDto,Transaction.class );
                transaction.setTrader( traderService.findByTraderId(traderId));
                traderService.buyFruit(transaction);

                String buyResponse = "BUY  " + transactionDto;
                return new ResponseEntity(buyResponse,HttpStatus.OK) ;
        }
        @GetMapping("/trader/{traderId}/profit")
        public ResponseEntity profit(@PathVariable String traderId) throws TraderNotFoundException {
                int profit = traderService.getProfit(Long.parseLong(traderId));
                String profitResponse = "PROFIT: " + profit;
                return new ResponseEntity(profitResponse, HttpStatus.OK) ;
        }

        @PostMapping("/trader/{traderId}/sell")
        public ResponseEntity sell(@PathVariable Long traderId, @Valid @RequestBody TransactionDto transactionDto) throws TraderNotFoundException, FruitNameNullException, NegativePriceException, NegativeWeightException, InsufficientFruitsAvailableException {
                transactionDto.setTraderId(traderId);
                Transaction transaction = modelMapper.map(transactionDto,Transaction.class );
                transaction.setTrader( traderService.findByTraderId(traderId));
                traderService.sellFruit(transaction);
                String sellResponse = "SOLD  " + transactionDto;
                return new ResponseEntity(sellResponse,HttpStatus.OK) ;
        }

}
