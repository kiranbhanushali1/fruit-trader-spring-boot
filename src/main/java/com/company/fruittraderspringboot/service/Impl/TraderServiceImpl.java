package com.company.fruittraderspringboot.service.Impl;

import com.company.fruittraderspringboot.exception.InsufficientFruitsAvailableException;
import com.company.fruittraderspringboot.exception.TraderNotFoundException;
import com.company.fruittraderspringboot.model.Trader;
import com.company.fruittraderspringboot.model.Transaction;
import com.company.fruittraderspringboot.repository.TraderRepository;
import com.company.fruittraderspringboot.repository.TransactionRepository;
import com.company.fruittraderspringboot.service.TraderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class TraderServiceImpl implements TraderService {

        @Autowired
        TraderRepository traderRepository;
        @Autowired
        TransactionRepository transactionRepository;

        @Override
        public Trader findByTraderId(Long id) throws TraderNotFoundException {
                return traderRepository.findByTraderId(id).orElseThrow(TraderNotFoundException::new);
        }

        @Override
        public Trader saveTrader(Trader trader) {
               return  traderRepository.save(trader);
        }

        @Override
        public void buyFruit( Transaction transaction) {
                transactionRepository.save(transaction);
        }

        @Override
        public Integer getProfit(Long traderId) throws TraderNotFoundException {
                return traderRepository.findByTraderId(traderId).orElseThrow(TraderNotFoundException::new).getProfit();
        }
        @Override
        public void updateProfit(Long traderId,Integer delta) throws TraderNotFoundException {
                Trader trader = traderRepository.findByTraderId(traderId).orElseThrow(TraderNotFoundException::new);
                trader.setProfit(trader.getProfit()+delta);
                traderRepository.save(trader);
        }


        @Override
        public void sellFruit( Transaction transactionToSell) throws TraderNotFoundException, InsufficientFruitsAvailableException {

                Trader trader = transactionToSell.getTrader();
                System.out.println(trader);
                Set<Transaction> transactionSet = trader.getTransactions();
                List<Transaction> transactionList = new ArrayList<>(transactionSet);
                transactionList.sort(Comparator.comparing(Transaction::getCreatedAt));

                //todo: replace this with custom query
                int total_weight = transactionList.stream().mapToInt(Transaction::getWeight).sum();
                if( total_weight < transactionToSell.getWeight()){
                        throw new InsufficientFruitsAvailableException();
                }

                int soldQuantity = 0 , soldPrice  = 0;
                Iterator<Transaction> iter = transactionList.iterator();
                List<Transaction> toRemove = new ArrayList<>();

                while(iter.hasNext()){
                        var transaction = iter.next();
                        if( soldQuantity + transaction.getWeight()  > transactionToSell.getWeight() ){
                                int remaining = transactionToSell.getWeight() - soldQuantity ;
                                // set remaining weight
                                transaction.setWeight( transaction.getWeight() -remaining);
                                soldPrice += remaining * transaction.getPrice();
                                transactionRepository.save(transaction);
                                soldQuantity += remaining ;
                                break;
                        }
                        soldQuantity += transaction.getWeight();
                        soldPrice += transaction.getPrice() * transaction.getWeight();
                        toRemove.add(transaction);
                }

                // profit calculation and updation
                int profit = (transactionToSell.getPrice()*transactionToSell.getWeight()) - soldPrice;
                updateProfit(trader.getTraderId(),profit);
                transactionRepository.deleteAll(toRemove);
        }
}
