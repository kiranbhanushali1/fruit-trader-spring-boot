package com.company.fruittraderspringboot.service;

import com.company.fruittraderspringboot.exception.InsufficientFruitsAvailableException;
import com.company.fruittraderspringboot.exception.TraderNotFoundException;
import com.company.fruittraderspringboot.model.Trader;
import com.company.fruittraderspringboot.model.Transaction;
import org.springframework.stereotype.Service;

@Service
public interface TraderService {
        Trader findByTraderId(Long id) throws TraderNotFoundException;
        Trader saveTrader(Trader trader);

        void buyFruit(Transaction transaction);

        void sellFruit(Transaction transaction) throws TraderNotFoundException, InsufficientFruitsAvailableException;

        Integer getProfit(Long traderId) throws TraderNotFoundException;
        void updateProfit(Long traderId, Integer delta) throws TraderNotFoundException;
}
