package com.company.fruittraderspringboot.model;

import com.company.fruittraderspringboot.exception.FruitNameNullException;
import com.company.fruittraderspringboot.exception.NegativePriceException;
import com.company.fruittraderspringboot.exception.NegativeWeightException;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.sun.istack.NotNull;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.util.Assert;

import javax.persistence.*;
import java.util.Date;
import java.util.Map;


@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Transaction {
    @Id
    @GeneratedValue
    private Long transactionId;

    private Integer price;
    private Integer weight;

    @CreationTimestamp
    private Date createdAt;

    @NotNull
    private String fruitName;

    @NotNull
    @ManyToOne(targetEntity = Trader.class,fetch = FetchType.LAZY,optional = false)
    private Trader trader;


}
