package com.company.fruittraderspringboot.model;

import com.company.fruittraderspringboot.exception.FruitNameNullException;
import com.company.fruittraderspringboot.exception.NegativePriceException;
import com.company.fruittraderspringboot.exception.NegativeWeightException;
import com.company.fruittraderspringboot.exception.TraderNameNullException;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.springframework.data.jpa.repository.Modifying;

import javax.persistence.*;
import java.util.Map;
import java.util.Set;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Trader {

    @Id
    @GeneratedValue
    private Long traderId;
    private String traderName;
    @Builder.Default
    private Integer profit=0;

    @JsonManagedReference
    @OneToMany(mappedBy = "trader", orphanRemoval = true)
    Set<Transaction> transactions;

    public static class TraderBuilder{
        public Trader.TraderBuilder customBuilder(Map<String,Object> body) throws TraderNameNullException {
            this.traderName = (String) body.get("traderName");
            if(this.traderName.length() ==0){
                throw new TraderNameNullException();
            }
            return this;
        }
    }

    @Override
    public String toString() {
        return "Trader{" +
                "traderId=" + traderId +
                ", trader_name='" + traderName + '\'' +
                ", profit=" + profit + '}';
    }
}
