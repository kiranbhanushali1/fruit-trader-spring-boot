package com.company.fruittraderspringboot.exception;

public class TraderNameNullException extends Exception {
        public TraderNameNullException(){
                super("Trader Name Can't Be Null");
        }
}
