package com.company.fruittraderspringboot.exception.handler;

import com.company.fruittraderspringboot.exception.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionHandler {

        @org.springframework.web.bind.annotation.ExceptionHandler({FruitNameNullException.class , InsufficientFruitsAvailableException.class, NegativePriceException.class, NegativeWeightException.class, TraderNotFoundException.class,TraderNameNullException.class})
        public ResponseEntity<Object> exceptionHandler(Exception e ) {
                return new ResponseEntity<>("error:"+e.getMessage(), HttpStatus.BAD_REQUEST);
        }
}
