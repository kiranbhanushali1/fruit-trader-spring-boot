package com.company.fruittraderspringboot.exception;

public class TraderNotFoundException extends Exception {
        public TraderNotFoundException() {
                super("Trader Not Found.");
        }

}
