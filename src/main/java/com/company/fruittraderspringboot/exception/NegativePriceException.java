package com.company.fruittraderspringboot.exception;

public class NegativePriceException extends Exception {
    public NegativePriceException() {
        super("Price Can't Be Negative.");
    }
}
