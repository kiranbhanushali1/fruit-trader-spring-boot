package com.company.fruittraderspringboot.exception;

public class FruitNameNullException extends Exception {
    public FruitNameNullException() {
        super("Fruit Name Can't Be Empty.");
    }
}
