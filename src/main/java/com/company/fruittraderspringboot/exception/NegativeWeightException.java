package com.company.fruittraderspringboot.exception;

public class NegativeWeightException extends Exception {
    public NegativeWeightException() {
        super("Weight Can't Be Negative.");
    }
}
