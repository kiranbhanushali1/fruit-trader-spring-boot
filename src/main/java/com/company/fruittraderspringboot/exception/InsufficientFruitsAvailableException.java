package com.company.fruittraderspringboot.exception;

public class InsufficientFruitsAvailableException extends Exception {
        public InsufficientFruitsAvailableException(){
                super("Insufficient Fruits Avalible.");
        }
}
