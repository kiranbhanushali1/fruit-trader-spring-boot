package com.company.fruittraderspringboot.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.context.annotation.Bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TransactionDto {
        @Min(value = 0,message = "Price Must be Non Negative")
        @JsonProperty("price")
        private Integer price;

        @Min(value = 0,message = "Weight Must be  Positive")
        @JsonProperty("weight")
        private Integer weight;

        @NotEmpty(message = "Fruit Name Can't Be Empty ")
        @JsonProperty("fruitName")
        private String fruitName;

        private  Long traderId;

        @Override
        public String toString() {
                return  weight + "KG  " + fruitName + " AT  " + price +  " RUPEES/KG.\n";
        }

}
