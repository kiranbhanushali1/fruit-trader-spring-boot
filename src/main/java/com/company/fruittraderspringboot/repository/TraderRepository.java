package com.company.fruittraderspringboot.repository;

import com.company.fruittraderspringboot.model.Trader;
import com.company.fruittraderspringboot.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TraderRepository  extends JpaRepository<Trader,Long> {
        public  Optional<Trader> findByTraderId(Long id);

}
