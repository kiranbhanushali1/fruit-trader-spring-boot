package com.company.fruittraderspringboot.repository;

import com.company.fruittraderspringboot.model.Trader;
import com.company.fruittraderspringboot.model.Transaction;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction,Long>{
        List<Transaction> findTransactionsByTrader(Trader trader, Sort sort);
}
